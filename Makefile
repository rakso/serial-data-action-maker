venv:
	python3 -m venv ./venv

requirements:
	pip freeze > requirements.txt

install:
	pip install -r requirements.txt