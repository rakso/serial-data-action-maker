# Serial data action maker

Script written in python to read data from serial port and e.g. saving it into file

##### Read more

- https://problemsolvingwithpython.com/11-Python-and-External-Hardware/11.04-Reading-a-Sensor-with-Python/
