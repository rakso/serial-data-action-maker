#!../venv/bin/python3
from sources.serialWeightFileDataWritter import SerialWeightFileDataWritter

if __name__ == "__main__":
    weightReader = SerialWeightFileDataWritter(
        'odczyty_z_wagi.csv', '/dev/ttyUSB1', 115200)
    weightReader.run()
