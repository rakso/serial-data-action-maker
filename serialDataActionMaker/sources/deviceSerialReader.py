#!../../venv/bin/python3
from serial import Serial


class DeviceSerialReader:
    def __init__(self, portName='/dev/ttyUSB0', baudrate=115200, serialTimeout=10, functionToCallWheneverLineIsReaded=print):
        self.ser = Serial()
        self.ser.baudrate = baudrate
        self.ser.port = portName
        self.ser.timeout = serialTimeout

        self.fireUpFunction = functionToCallWheneverLineIsReaded
        self.fireUpFunctionKwargs = None

    def setFireUpFunction(self, function, **kwargs):
        self.fireUpFunction = function
        self.fireUpFunctionKwargs = kwargs

    def open(self):
        self.ser.open()
        if self.ser.is_open:
            print('Serial is opened!')

    def readLine(self):
        try:
            line = self.ser.readline()
        except KeyboardInterrupt:
            print(' Interrupted! Bye!')
            exit(0)
        return line

    def loop(self):
        while True:
            bytesData = self.readLine()
            # bytesData.decode() converts bytes data into string, which contains white chars like \r\n which are cleared by rstrip()
            dataStripped = bytesData.decode().rstrip()
            if (self.fireUpFunctionKwargs == None):
                self.fireUpFunction(dataStripped)
            else:
                self.fireUpFunction(dataStripped, **self.fireUpFunctionKwargs)


if __name__ == "__main__":
    def xD(dataFromSerial, **kwargs):
        test = kwargs.get('test2', "test2 var not defined")
        test4 = kwargs.get('test3', "test3 var not defined")
        print('DANE', test, test4)

        print(dataFromSerial)

    serialReader = DeviceSerialReader(
        '/dev/ttyUSB1')
    serialReader.setFireUpFunction(xD, test2='test2', test3='test3')
    serialReader.open()
    serialReader.loop()
