#!../../venv/bin/python3
from time import time
from datetime import datetime
from .deviceSerialReader import DeviceSerialReader


class SerialWeightFileDataWritter:
    def __init__(self, fileName, serialPort, baudrate):
        self.fileName = self.generateName(fileName)

        self.serialReader = DeviceSerialReader(
            portName=serialPort, baudrate=baudrate, serialTimeout=10)

        self.keywordForSearch = 'Waga: '
        self.currentlyReadedLineOfText = ''

        self.appendToFile('currentTimestamp,weight\n')

    def generateName(self, rawName):
        now = datetime.now()  # current date and time
        dateTimeString = now.strftime("%m-%d-%Y_%H:%M:%S")

        indexOfTheLastDot = rawName.rfind('.')
        fileName = rawName[:indexOfTheLastDot]
        fileExtension = rawName[indexOfTheLastDot+1:]
        return "%s_%s.%s" % (fileName, dateTimeString, fileExtension)

    def getCurentTimestamp(self):
        return time()

    def checkForKeyword(self):
        return self.keywordForSearch in self.currentlyReadedLineOfText

    def splitText(self):
        return self.currentlyReadedLineOfText.split()

    def appendToFile(self, content):
        f = open(self.fileName, "a")
        f.write(content)
        f.close()

    def checkAndWrite(self, lineOfText):
        print('Data: ', lineOfText)
        self.currentlyReadedLineOfText = lineOfText
        if self.checkForKeyword():
            splittedText = self.splitText()
            textToSave = "%f,%s\n" % (
                self.getCurentTimestamp(), splittedText[1])
            self.appendToFile(textToSave)

    def run(self):
        self.serialReader.setFireUpFunction(self.checkAndWrite)
        self.serialReader.open()
        self.serialReader.loop()


if __name__ == "__main__":
    weightReader = SerialWeightFileDataWritter(
        'odczyty_z_wagi.csv', '/dev/ttyUSB1', 115200)
    weightReader.run()
